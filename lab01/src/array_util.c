#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "array_util.h"
#include <time.h>


uint64_t *array_init(const size_t len){
    uint64_t* array;
    size_t i;
    
    if(len <= 0)
        return NULL;
    
    srand(time(0));
    
    // Allocation mémoire pour le tableau entier
    array = (uint64_t*)malloc(len * sizeof(uint64_t));
    
    // Attribue une valeur aléatoire pour chaque index du tableau.
    for(i = 0; i < len; i++){
        array[i] = (uint64_t)(rand());
    }
    
    return array;
}


void array_clear(uint64_t *data){
    if(data)
        free(data);
}


// Les fonctions "merge" et "mergeSort" ci-dessous ont été reprise du 
// site suivant :
// https://www.geeksforgeeks.org/merge-sort-for-doubly-linked-list/
// Et modifier pour correspondre à notre structure.


// Merges two subarrays of arr[]. 
// First subarray is arr[l..m] 
// Second subarray is arr[m+1..r] 
void merge(uint64_t* arr, uint64_t l, uint64_t m, uint64_t r) 
{ 
    uint64_t i, j, k; 
    uint64_t n1 = m - l + 1; 
    uint64_t n2 =  r - m; 
  
    /* create temp arrays */
    uint64_t L[n1], R[n2]; 
  
    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++) 
        L[i] = arr[l + i]; 
    for (j = 0; j < n2; j++) 
        R[j] = arr[m + 1+ j]; 
  
    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray 
    j = 0; // Initial index of second subarray 
    k = l; // Initial index of merged subarray 
    while (i < n1 && j < n2) 
    { 
        if (L[i] <= R[j]) 
        { 
            arr[k] = L[i]; 
            i++; 
        } 
        else
        { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 
  
    /* Copy the remaining elements of L[], if there 
       are any */
    while (i < n1) 
    { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    /* Copy the remaining elements of R[], if there 
       are any */
    while (j < n2) 
    { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    } 
}

  
/* l is for left index and r is right index of the 
   sub-array of arr to be sorted */
void mergeSort(uint64_t* arr, uint64_t l, uint64_t r) 
{ 
    if (l < r) 
    { 
        // Same as (l+r)/2, but avoids overflow for 
        // large l and h 
        uint64_t m = l+(r-l)/2; 
  
        // Sort first and second halves 
        mergeSort(arr, l, m); 
        mergeSort(arr, m+1, r); 
  
        merge(arr, l, m, r); 
    } 
} 



void array_sort(uint64_t *data, const size_t len){
    mergeSort(data, 0, len-1);
}



void displayArray(uint64_t* array, const size_t len){
    size_t i;
    
    for(i = 0; i < len; i++)
            printf("%ld\n", array[i]);
}
