#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "list_util.h"


struct list_element{
    struct list_element* prev;
    struct list_element* next;
    uint64_t value;
};


struct list_element *list_init(size_t len){
    
    struct list_element* head;
    struct list_element* iterator;
    size_t i;
    
    if(len <= 0)
        return NULL;
    
    srand(time(0));
    
    // allocation mémoire pour notre sctuture head (pas de vérification en cas d'erreur).
    head = (struct list_element*)malloc(sizeof(struct list_element));
    
    head->value = (uint64_t)(rand());       // Attribution d'une valeur aléatoire
    
    iterator = head;
    
    
    for(i = 1; i < len; i++){
        
        // Crée une nouvelle strcuture pour l'élément suivant de la liste.
        iterator->next = (struct list_element*)malloc(sizeof(struct list_element));
        
        // Fait pointer la nouvelle structure sur la structure actuelle
        iterator->next->prev = iterator;
        
        iterator = iterator->next;
        
        iterator->value = (uint64_t)(rand());
    }
    
    return head;
}



void list_clear(struct list_element *head){
    struct list_element* iterator;
    
    while(head){
        iterator = head;
        head = head->next;
        free(iterator);
    }
}




// Les fonctions merge et mergeSort ont été reprises du site suivant :
// https://www.geeksforgeeks.org/merge-sort-for-doubly-linked-list/
// et modifiée un petit peu pour pouvoir fonctionner dans notre cas.

static struct list_element *merge(struct list_element *first, struct list_element *second) 
{ 
    // If first linked list is empty 
    if (!first) 
        return second; 
  
    // If second linked list is empty 
    if (!second) 
        return first; 
  
    // Pick the smaller value 
    if (first->value < second->value) 
    { 
        first->next = merge(first->next,second); 
        first->next->prev = first; 
        first->prev = NULL; 
        return first; 
    } 
    else
    { 
        second->next = merge(first,second->next); 
        second->next->prev = second; 
        second->prev = NULL; 
        return second; 
    } 
}



static struct list_element *mergeSort(struct list_element *head)
{ 
    struct list_element *middle;
    struct list_element *it;
    
    if (!head || !head->next)
        return head;
    
    middle = head;
    it     = head;
    
    // split la liste en deux
    while(it->next && it->next->next){
        it = it->next->next;
        middle = middle->next;
    }
    
    middle = middle->next;
    middle->prev->next = NULL;
    

    // Sort first and second halves 
    head = mergeSort(head); 
    middle = mergeSort(middle);

    return merge(head, middle);
}




void list_sort(struct list_element **head){
    *head = mergeSort(*head);
}



void displayList(struct list_element *head){
    struct list_element* iterator;
    
    iterator = head;
    
    while(iterator){
        printf("%ld\n", iterator->value);
        iterator = iterator->next;
    }
}
